/*
 * Модуль: электронный дневник
 */
 
void assembly::p_journal(string url)
{
	container js,body;

		js.put("<script type='text/javascript' src='http://localhost/javascripts/userlayer-journal.js'></script> ");
		
		body.put("<div class='wrap'>");
			body.put("<div class='bar' style='font: 13px arial'>");
				body.put("<div style='border-bottom: 1px solid #E4EAEF; vertical-align: bottom; padding-bottom: 3px; position: relative;'>");
					body.put("<input type='text' size='32' class='quicksearch' name='quicksearch' value='Быстрый поиск' title='Быстрый поиск' />");
					body.put("<p class='sort'>");
					body.put("<span class='found'></span>");
					body.put("Сортировать по: ");
					body.put("<label rel='date:desc' class='active'>дате<small>&#9650;</small></label>");
					body.put("</p>");
				body.put("</div>");
			body.put("</div>");
			body.put("<div class='content-wrap'>");
				body.put("<div class='pager-wrap'><ul class='pager'></ul></div>");
				body.put("<div class='content'></div>");
				body.put("<div class='filters'></div>");
				body.put("<div style='clear:both'></div>");
				body.put("<div class='pager-wrap'><ul class='pager bottom'></ul></div>");
				body.put("<div id='loader'></div>");
			body.put("</div>");
//			body.put("<div class='note'> ");
//				body.put("<p></p> ");
//			body.put("</div> ");
		body.put("</div> ");
		
	content::message html;
	html.sys_name="journal";
	html.title="Электронный дневник - ИТ-Школа";
	html.text="Дневник";
	html.head="";
	html.js=js.get();
	html.body=body.get();
	html.html="";
	http_console_log(html.sys_name);
	render("message",html);
};

void assembly::p_journal_json(string url)
{
	container body;
	ostringstream sstr;
	
	unsigned long long id,eid;
	time_converter conv;

	struct field_tb_public {
		string fio;
		string alias;
		string name;
		int t_class_id;
		string class_id;
	};
	
	struct field_tb_register {
		unsigned long long t_date;
		unsigned long long t_date_post;
		int p_id;
		string id;
		string fio;
		string obj_name;
		string theme;
		string classwork;
		string homework;
		string date;
		string date_post;
		string array_value;
	};
	
	struct field_tb_register_value {
		int t_value;
		string value;
		string comments;
	};

	db << "SELECT \
		   tb_public.fio, tb_list_class.alias, tb_list_class.name, tb_public.class_id \
		   FROM tb_public \
		   left JOIN tb_list_class ON tb_list_class.id = tb_public.class_id \
		   WHERE `tb_public`.`id` = 1";
		  
	db.fetch(results);
	
	field_tb_public *field_public = new field_tb_public[1];
	
	id=0;
	while(results.next(rows)) 
	{   
		rows >> field_public[id].fio >> field_public[id].alias >> 
				field_public[id].name >> field_public[id].t_class_id;
		
		sstr<<field_public[id].t_class_id;
		field_public[id].class_id=sstr.str();
		sstr.str("");sstr.str().empty();
		id++;
	}

	db << "SELECT \
		   tb_register_lesson.id, tb_staff.fio, tb_list_obj.name, \
		   tb_register_lesson.theme, tb_register_lesson.classwork, tb_register_lesson.homework, \
		   UNIX_TIMESTAMP(tb_register_lesson.date), UNIX_TIMESTAMP(tb_register_lesson.date_post) \
		   FROM tb_register_lesson \
		   left JOIN tb_list_obj ON tb_list_obj.id = tb_register_lesson.obj_id \
		   left JOIN tb_staff ON tb_staff.id = tb_register_lesson.teacher_id \
		   WHERE tb_register_lesson.class_id ="+field_public[0].class_id;
		  
	db.fetch(results);
	
	field_tb_register *field_obj = new field_tb_register[results.rows()];
	 
	id=0;
	while(results.next(rows)) 
	{   
		rows >> field_obj[id].p_id >> field_obj[id].fio >> field_obj[id].obj_name >> 
				field_obj[id].theme >> field_obj[id].classwork >> field_obj[id].homework >> 
				field_obj[id].t_date >> field_obj[id].t_date_post;
		
		sstr<<field_obj[id].p_id;
		field_obj[id].id=sstr.str();
		sstr.str("");sstr.str().empty();
		
		///////////////////////////////////////////////////////////////////////////////
		
		db << "SELECT \
			   value,comments \
			   FROM tb_register_value \
			   WHERE lesson_id = "+field_obj[id].id+" and pupil_id = "+"1"+"";
		
		result embedded; 
		db.fetch(embedded); 
		field_tb_register_value *field_value = new field_tb_register_value[embedded.rows()]; 
				
		eid=0;
		while(embedded.next(rows)) 
		{   
			rows >> field_value[eid].t_value >> field_value[eid].comments;
			
			sstr<<field_value[eid].t_value;
			field_value[eid].value=sstr.str();
			sstr.str("");sstr.str().empty();
			
			if (field_value[eid].value == "0") field_value[eid].value = "n";
			
			field_obj[id].array_value=field_value[eid].comments +": "+ field_value[eid].value + "<br>";
			
			eid++;
		}
	
		///////////////////////////////////////////////////////////////////////////////
				
		field_obj[id].date=conv.convert(field_obj[id].t_date,dmY);
		field_obj[id].date_post=conv.convert(field_obj[id].t_date_post,dmY);

		id++;
	}

	body.put("[");
	
	for(unsigned long long i=0; i<id; i++)
	{
		body.put("{");
		body.put("\"id\":\""+field_obj[i].id+"\",");
		body.put("\"fio\":\""+field_obj[i].fio+"\",");
		body.put("\"obj_name\":\""+field_obj[i].obj_name+"\",");
		body.put("\"theme\":\""+field_obj[i].theme+"\",");
		body.put("\"array_value\":\""+field_obj[i].array_value+"\",");
		body.put("\"classwork\":\""+field_obj[i].classwork+"\",");
		body.put("\"homework\":\""+field_obj[i].homework+"\",");
		body.put("\"date\":\""+field_obj[i].date+"\",");
		body.put("\"date_post\":\""+field_obj[i].date_post+"\"");
		body.put("}");
		if(i<id-1) 
		{
			body.put(",");
		}
	}
	
	body.put("]");
	
	content::message html;
	html.sys_name="journal_json";
	http_console_log(html.sys_name);
	response().out() << body.get();
};
