var findOneOrCreate = require('mongoose-find-one-or-create');

module.exports = function (mongoose, models)
{
  var collection = 'contests', Schema = mongoose.Schema;

  var schema = new Schema({
      name: String                                         // наименование
    , age: String                                          // возрастные ограничения
    , cost: Number                                         // стоимость
    , status: String                                       // статус
    , poster: String                                       // изображение
    , description: String                                  // описание
    , level: String                                        // уровень мероприятия
    , formats: [String]                                    // тип присылаемых работ
    , tags: [String]                                       // направления
    , requirements: [String]                               // требования к работам
    , startday: { type: Date, default: Date.now() }       // дата начала приема заявок
    , endday: { type: Date, default: Date.now() }         // дата окончания приема заявок
    , resultday: { type: Date, default: Date.now() }      // дата подведения итогов
  });
  
  schema.virtual('contestId').get(function() {
      return this._id.toString();
  });

	schema.plugin(findOneOrCreate);

  this.model = mongoose.model(collection, schema);

  return this;
};