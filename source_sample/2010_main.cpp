#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <time.h>

#include <cppcms/application.h>  
#include <cppcms/applications_pool.h>  
#include <cppcms/url_dispatcher.h>
#include <cppcms/http_response.h>
#include <cppcms/http_request.h>
#include <cppcms/base_view.h>  
#include <cppcms/service.h>  
#include <cppcms/form.h> 
#include <dbixx/dbixx.h>  

using namespace cppcms;
using namespace dbixx; 
using namespace std;

#include "constant.h"
#include "function.h"
#include "content.h"
#include "class.h"

session db("mysql");  

class assembly:public application 
{  
	public:  
		assembly(cppcms::service &srv):application(srv){

		};  
		
		virtual void main(string url);  
		
		virtual void p_404(string url);
		virtual void p_about(string url);
		virtual void p_contact(string url);
		virtual void p_service(string url);
		  
		virtual void p_index(string url);  
		virtual void p_timetable(string url); 
		virtual void p_register(string url);
		virtual void p_register_json(string url);
		virtual void p_journal(string url);  
		virtual void p_journal_json(string url);  
		virtual void p_staff(string url);  
		virtual void p_staff_json(string url);  
		virtual void p_library(string url);  
		virtual void p_library_json(string url);  
		virtual void p_public(string url); 
		virtual void p_public_json(string url); 
};

#include "module/system.h"

#include "module/index.h"
#include "module/timetable.h"
#include "module/register.h"
#include "module/journal.h"
#include "module/staff.h"
#include "module/library.h"
#include "module/public.h"

void assembly::main(string url)
{
	if(url=="/") assembly::p_index(url);
	else if(url=="/index") assembly::p_index(url);
	else if(url=="/timetable") assembly::p_timetable(url);
	else if(url=="/register") assembly::p_register(url);
	else if(url=="/register/json") assembly::p_register_json(url);
	else if(url=="/journal") assembly::p_journal(url);
	else if(url=="/journal/json") assembly::p_journal_json(url);
	else if(url=="/staff") assembly::p_staff(url);
	else if(url=="/staff/json") assembly::p_staff_json(url);
	else if(url=="/library") assembly::p_library(url);
	else if(url=="/library/json") assembly::p_library_json(url);
	else if(url=="/public") assembly::p_public(url);
	else if(url=="/public/json") assembly::p_public_json(url);
	else if(url=="/about") assembly::p_about(url);
	else if(url=="/contact") assembly::p_contact(url);
	else if(url=="/service") assembly::p_service(url);
	else assembly::p_404(url);
};

int main(int argc,char ** argv)
{
    try 
    {
		#include "config/db.h"
		
        service srv(argc,argv);
        srv.applications_pool().mount(applications_factory<assembly>());
        srv.run();
    }
    
    catch(exception const &e) 
    {
        cerr << e.what() << endl;
    }
};
