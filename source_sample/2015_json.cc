#include <stdio.h>
#include "pstream.h"

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/json_parser.hpp>  
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <iostream>
#include <string>

boost::property_tree::ptree pt;
std::stringstream buffer_xml, buffer_json;

int parse_output(const char *command) 
{
    std::string output;
    char buffer[128];
    FILE *fp;

    if ((fp = popen(command, "r")) == NULL) 
    {
        std::cout <<  "Error opening pipe!" << std::endl;
        return -1;
    }

    buffer_xml << "<reply>";

    while (fgets(buffer, 128, fp) != NULL) {
        buffer_xml << "<dev>" <<  buffer << "</dev>";
    }

    buffer_xml << "</reply>";

    if(pclose(fp))  {
        std::cout <<  "Command not found or exited with error status" << std::endl;
        return -1;
    }

    boost::property_tree::read_xml(buffer_xml, pt);
    boost::property_tree::write_json(buffer_json, pt);
    output = buffer_json.str();
    
    boost::replace_all(output, "\\n", "");
    
    //std::cout << buffer_xml;

    return 0;
}

int main(int argc, char **argv)
{
    std::string buffer, output, temp, temp2;
    std::size_t pos1, pos2;
    
    char buffer_cmd[255];
    
    std::string argv1 = argv[1];
    
    if (argv1 == "--list")
    {
        parse_output("ls /dev | grep sd | awk 'length($0) < 4'");
    }
    else
    {
        std::string argv2 = argv[2];
        
        redi::pstream proc("fdisk " + argv1, redi::pstreams::pstdout | redi::pstreams::pstdin | redi::pstreams::pstderr);

        while (proc >> buffer){
            if (buffer.find("help") != std::string::npos)
                break;
        }
        
        if (argv2 == "p")
        {
            proc << argv[2] << std::endl;
            
            output = "<reply>";
            
            while (proc >> buffer)
            {
                output = output + buffer + " ";
                if (buffer.find("help") != std::string::npos || buffer.find("default") != std::string::npos) break;
            }
            
            boost::replace_first(output, "Disk ", "<disk>");
            boost::replace_first(output, ": ", "</disk><info><size>"); 
            boost::replace_first(output, ", ", "</size><bytes>");
            
            boost::replace_first(output, " bytes ", "</bytes><heads>");
            boost::replace_first(output, " heads, ", "</heads><sectors>");
            boost::replace_first(output, " sectors/track, ", "</sectors><cylinders>");
            boost::replace_first(output, " cylinders, total ", "</cylinders><total>");
            boost::replace_first(output, " sectors ", "</total>");
            
            boost::replace_first(output, "Units = ", "<units>");

            boost::replace_first(output, " Disk identifier: ", "</units><id>");
            boost::replace_first(output, " Device Boot Start End", "</id></info>");
            
            boost::replace_first(output, " Blocks Id System ", "<sd>");

            boost::replace_last(output, "Command (m for help):", "");
            
            boost::trim(output);
            
            temp2 = output.substr(output.find("<sd>") + 4);
            boost::replace_first(output, temp2, "");
            
            while (temp2.length() > 0)
            {
                pos1 = temp2.find(" ");
                temp = temp2.substr(0, pos1);
                temp2 = temp2.substr(pos1 + 1);
                output = output + "<part><device>" + temp + "</device>";
                
                pos1 = temp2.find(" ");
                temp = temp2.substr(0, pos1);
                temp2 = temp2.substr(pos1 + 1);
                
                if (temp == "*") 
                {
                    pos1 = temp2.find(" ");
                    temp = temp2.substr(0, pos1);
                    temp2 = temp2.substr(pos1 + 1);
                    output = output + "<boot>yes</boot><start>" + temp + "</start>";
                }
                else
                {
                    output = output + "<boot>no</boot><start>" + temp + "</start>";
                }
                
                pos1 = temp2.find(" ");
                temp = temp2.substr(0, pos1);
                temp2 = temp2.substr(pos1 + 1);
                output = output + "<end>" + temp + "</end>";
                
                pos1 = temp2.find(" ");
                temp = temp2.substr(0, pos1);
                temp2 = temp2.substr(pos1 + 1);
                output = output + "<blocks>" + temp + "</blocks>";
                
                pos1 = temp2.find(" ");
                temp = temp2.substr(0, pos1);
                temp2 = temp2.substr(pos1 + 1);
                output = output + "<id>" + temp + "</id><system>";
                
                while (temp2.length() > 0)
                {
                    pos1 = temp2.find(" ");
                    
                    if (pos1 == std::string::npos)
                    {
                        temp = temp2.substr(0);
                        temp2 = "";
                    }
                    else
                    {
                        temp = temp2.substr(0, pos1);
                    }
                    
                    if (temp.find("dev") == std::string::npos)
                    {
                        temp2 = temp2.substr(pos1 + 1);
                        output = output + temp + " ";
                    }
                    else
                    {
                        break;
                    }
                }
                
                boost::trim(output);
                
                output = output + "</system></part>";
            }
            
            output = output + "</sd></reply>";
            
            buffer_xml << output;
            boost::property_tree::read_xml(buffer_xml, pt);
            boost::property_tree::write_json(buffer_json, pt);
            std::cout << buffer_json.str();
            
            //std::cout << output;
            
            proc << "q" << std::endl;
        }
        else if (argv2 == "v")
        {
            proc << argv[2] << std::endl;
            
            output = "<reply><text>";
            
            while (proc >> buffer)
            {
                output = output + buffer + " ";
                if (buffer.find("help") != std::string::npos || buffer.find("default") != std::string::npos) break;
            }                   
            
            boost::replace_last(output, "Command (m for help):", "");
            boost::trim(output);
            
            output += "</text></reply>";;
            
            buffer_xml << output;
            boost::property_tree::read_xml(buffer_xml, pt);
            boost::property_tree::write_json(buffer_json, pt);
            std::cout << buffer_json.str();
            
            //std::cout << output;
            
            proc << "q" << std::endl;
        }
        //else if (argv2 == "l")
        //{
        //    proc << argv[2] << std::endl;
        //    
        //    while (proc >> buffer)
        //    {
        //        output = output + buffer + " ";
        //        if (buffer.find("help") != std::string::npos || buffer.find("default") != std::string::npos) break;
        //    }
        //    
        //    std::cout << output;
        //}
        else
        {
            for (int i = 2; i < argc; i++)
            {
                proc << argv[i] << std::endl;
                sleep(1);
            }
        }
    }
    
    return 0;
} 