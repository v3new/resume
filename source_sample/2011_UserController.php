<?php

namespace Acme\HinduBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\SessionStorage\SessionStorageInterface;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class UserController extends Controller
{
  public function indexAction()
  {
  
    $session = $this->getRequest()->getSession();
    $session->start();
    
    $user = $this->getDoctrine()->getRepository('AcmeHinduBundle:Users')->findOneByUser_Id($session->get('user_id'));
    if (!$user) 
    {
      throw $this->createNotFoundException('Пользователя нет');
    }
    
    $user_orders = $this->getDoctrine()->getRepository('AcmeHinduBundle:Orders')->findByOrderUserId($session->get('user_id'));
    if (!$user_orders) 
    {
      $orders = array('error' => true);
    }
    else 
    {
      foreach ($user_orders as $item) {
      
        $cafe_tables = $item->getOrderTables();
        $cafe = $cafe_tables->getCafeTables();

        $array_orders[] = array(
          'title' => $cafe->getLabel(),
          'cafe_id' => $cafe->getCafeId(),
          'address' => $cafe->getAddress(),
          'cafePhone' => $cafe->getcafePhone(),
          'countPerson' => $cafe_tables->getTablesCountPerson(),
          'orderStatus' => $item->getorderStatus(),	
          'orderTime' => $item->getOrderTime(),	
          'orderTimeStrf' => strftime("%d %b, %A", $item->getOrderTime()->getTimestamp()),	
          'orderUserId' => $item->getOrderUserId(),
        );
      }
      $orders = array('error' => false, 'content' => $array_orders);
    }
    
    return $this->render('AcmeHinduBundle:Default:user_profile.html.twig', array(
        'user' => $user,
        'orders' => $orders,
        'now' => time()
        ));
  }
  
  public function editAction()
  {
  
    $session = $this->getRequest()->getSession();
    $session->start();
    
    $request = $this->get('request');

    $user = $this->getDoctrine()->getRepository('AcmeHinduBundle:Users')->findOneByUser_Id($session->get('user_id'));
    if (!$user) 
    {
        throw $this->createNotFoundException('Пользователя нет');
    }
    return $this->render('AcmeHinduBundle:Default:edit_profile.html.twig', array('user' => $user));
  }
  
  public function ajaxAction($id)
  {
  
    $session = $this->getRequest()->getSession();
    $session->start();
  
    $request = Request::createFromGlobals();
    $em = $this->getDoctrine()->getEntityManager();
    
    if ($id == 'main') 
    {
      $user = $this->getDoctrine()->getRepository('AcmeHinduBundle:Users')->findOneByUser_Id($session->get('user_id'));
      if (!$user) 
      {
        throw $this->createNotFoundException('Пользователя нет');
      }
      $date = $request->request->get('year').'-'.$request->request->get('month').'-'.$request->request->get('day').' 00:00:00';

      $user->setUserName($request->request->get('name'));
      $user->setUserSex($request->request->get('sex'));
      $user->setUserBdate(new \DateTime($date));
      $em->flush();
      
      return new Response('Данные сохранены.');
    }
    
    if ($id == 'pass') 
    {
      if ($request->request->get('new1') != $request->request->get('new2')) 
      {
        return new Response('Новые пароли не совпадают.');
      }
      
      $user = $this->getDoctrine()->getRepository('AcmeHinduBundle:Users')->findOneByUser_Id($session->get('user_id'));
      if ($user->getPassword() != $request->request->get('old')) 
      {
        return new Response('Старый пароль не верен.');
      }
      
      $user->setPassword($request->request->get('new1'));
      $em->flush();
      
      return new Response('Пароль обновлен.');
    }
   /* Добавляем-обновляем фотку */ 
	if ($id == 'photo') 
  {
    $user = $this->getDoctrine()->getRepository('AcmeHinduBundle:Users')->findOneByUser_Id($session->get('user_id'));
    if (!$user) 
    {
      throw $this->createNotFoundException('Пользователя нет');
    }

    $path_thumbs = "/var/www/grant-hindu-soft/web/bundles/hindugrant/uploads/";		
    $img_thumb_width = 192;
    $extlimit = "yes";
    $limitedext = array(".gif",".jpg",".png",".jpeg",".bmp");		

    $file_type = $_FILES['photo']['type'];
    $file_name = $_FILES['photo']['name'];
    $file_size = $_FILES['photo']['size'];
    $file_tmp = $_FILES['photo']['tmp_name'];

    if(!is_uploaded_file($file_tmp)){
      return new Response('error');
      exit(); 
    }

    $ext = strrchr($file_name,'.');
    $ext = strtolower($ext);
    if (($extlimit == "yes") && (!in_array($ext,$limitedext))) {
      return new Response('error');
      exit();
    }

    $getExt = explode ('.', $file_name);
    $file_ext = $getExt[count($getExt)-1];
    $rand_name = md5(time());
    $rand_name= rand(0,999999999);
    $ThumbWidth = $img_thumb_width;

    if($file_size){
      if($file_type == "image/pjpeg" || $file_type == "image/jpeg"){
        $new_img = imagecreatefromjpeg($file_tmp);
      }elseif($file_type == "image/x-png" || $file_type == "image/png"){
        $new_img = imagecreatefrompng($file_tmp);
      }elseif($file_type == "image/gif"){
        $new_img = imagecreatefromgif($file_tmp);
      }

      list($width, $height) = getimagesize($file_tmp);

      $imgratio=$width/$height;
      if ($imgratio>1){
        $newwidth = $ThumbWidth;
        $newheight = $ThumbWidth/$imgratio;
      }else{
        $newwidth = $ThumbWidth;
        $newheight = $ThumbWidth/$imgratio;
      }

      $resized_img = imagecreatetruecolor($newwidth,$newheight);
      imagecopyresized($resized_img, $new_img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
      ImageJpeg ($resized_img,"$path_thumbs/$rand_name.$file_ext");
      ImageDestroy ($resized_img);
      ImageDestroy ($new_img);
      
      $user->setUserPhoto('/bundles/hindugrant/uploads/'.$rand_name.'.'.$file_ext);
      $em->flush();
      
      return new Response('/bundles/hindugrant/uploads/'.$rand_name.'.'.$file_ext);
    }
	}
	
    if ($id == 'confirm') 
    {
      if (!preg_match ("/^[0-9]{10}$/", $request->request->get('tel'))) 
      {
        return new Response('Проверьте номер телефона.');
      }
      
      $session = $this->getRequest()->getSession();
      $session->start();
      $session->set('sms_code', mt_rand(10000, 99999));
      $session->set('phone_number', $request->request->get('tel'));
      
      
      $login='u2932';
      $pass='7Ygbhu';
      
      $sender='bronestol';
      $sms='Код подтверждения: '.$session->get('sms_code');

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"http://clients.smstower.ru/sender.v2.php");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "sender=$sender&password=$pass&login=$login&sms=$sms&phone=7".$request->request->get('tel'));
      
      $data = curl_exec($ch);
      if (curl_errno($ch)) {
          die("Error: " . curl_error($ch));
      }else{
        curl_close($ch);
        $xml = simplexml_load_string($data);
      }

      $em->flush();
      
      return new Response('СМС код выслан.');
    }
    
    if ($id == 'phone') 
    {
      $session = $this->getRequest()->getSession();
      $session->start();
      
      if ($session->get('sms_code') != $request->request->get('code')) 
      {
        return new Response('СМС код не подходит.');
      }
      
      if ($session->get('phone_number') != $request->request->get('tel')) 
      {
        return new Response('Повторите отправку СМС кода.');
      }
     
      $user = $this->getDoctrine()->getRepository('AcmeHinduBundle:Users')->findOneByUser_Id($session->get('user_id'));
      $user->setUserPhone($request->request->get('tel'));
      
      $em->flush();
      
      return new Response('Телефон обновлен.');
    }
    return new Response('');
  }
}