<?

  include('../configs/config.php');
  include('../libs/dbsimple.php');
  include('../libs/function.php');
  include('../libs/wmxml.php');

  $count=0;

  $rows = $db->select('SELECT `wmr`,`check`,`fio` FROM `nat`');

  $r = _WMXML9();
  if ($r['retval'] == 0)
  {
    while(list($key,$val) = each($r['purses'])) 
    {
      $balance[$key] = $val;
    }
  }

  foreach ($rows as $numRow => $row) 
  {
    if (isset($balance[$row['wmr']]) && $balance[$row['wmr']] > 0)
    {
      $datestart = date('Ymd H:i:s', mktime(0, 0, 0, date('m')-1, date('d'), date('Y')));
      if ($row['check']!='0000-00-00 00:00:00') $datestart = str_replace('-', '', $row['check']);
      
      $r = _WMXML3($row['wmr'],'','','','',$datestart,date('Ymd H:i:s'));

      if ($r['retval'] == 0)
      {
        $amount = 0;
        $date = 0;

        while(list($key,$val) = each($r['operations'])) 
        {
          if ($val['corrwmid'] != WMID && $val['type'] == "in")
          {
            $amount += $val['amount'];
            $date = $val['datecrt'];
          }
        }
                
        $date = date("Y-m-d H:i:s", strtotime(substr_replace(substr_replace($date,"-",6,0),"-",4,0) . "+1 second"));

        if ($amount > 0)
        {
          $tranid = $db->selectcell('SELECT `id` FROM `log` ORDER BY `id` DESC LIMIT 1');
          $desc = "NAT ".iconv("UTF-8", "CP1251", $row['fio'])." ".$date;
          $r = _WMXML2($tranid,$row['wmr'],PURSE,$amount,'0','',$desc,'0','1');
        }

        $amount = floor($amount);

        if ($amount > 0)
        {
          $db->query('UPDATE nat SET balance=balance+?d, `check`=? WHERE wmr=? limit 1', $amount, $date, $row['wmr']);
          $db->query('INSERT INTO log(event) VALUES(?)', "auto to nat ".$row['wmr']." add ".$amount." rur.");
          $count += 1;
        }
      }
    }
  }

  echo $count;
 
?>