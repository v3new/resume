# Первичная настройка сети
ifconfig
cd /etc/network
nano interfaces
>>
#########################################
auto lo eth0 eth1 eth2

iface lo inet loopback

# WI-FI
allow-hotplug eth0
iface eth0 inet static
	address 192.168.0.1
	netmask 255.255.255.0
	broadcast 192.168.0.255
	network 192.168.0.0

# TOMTEL
allow-hotplug eth1
iface eth1 inet static
	address 
	netmask 255.255.255.128
	broadcast 
	network 
	gateway 
	dns-nameservers 77.106.109.135 77.106.108.139
	
# TTK
allow-hotplug eth2
iface eth2 inet dhcp
  pre-up /vpn/system/pre-up-ttk-eth2
  post-down /vpn/system/post-down-ttk-eth2

# VPN
  iface ppp0 inet ppp
  provider south
  pre-up /vpn/system/pre-up-ppp0
  post-down /vpn/system/post-down-ppp0
#############################################################
<<
/etc/init.d/networking restart

# Редактирование репозиториев
cd /etc/apt
nano sources.list
>>
#############################################################
################### OFFICIAL UBUNTU REPOS ###################
#############################################################

###### Ubuntu Main Repos
deb http://us.archive.ubuntu.com/ubuntu/ lucid main restricted universe multiverse 
deb-src http://us.archive.ubuntu.com/ubuntu/ lucid main restricted universe multiverse 

###### Ubuntu Update Repos
deb http://us.archive.ubuntu.com/ubuntu/ lucid-security main restricted universe multiverse 
deb http://us.archive.ubuntu.com/ubuntu/ lucid-updates main restricted universe multiverse 
deb http://us.archive.ubuntu.com/ubuntu/ lucid-proposed main restricted universe multiverse 
deb http://us.archive.ubuntu.com/ubuntu/ lucid-backports main restricted universe multiverse 
deb-src http://us.archive.ubuntu.com/ubuntu/ lucid-security main restricted universe multiverse 
deb-src http://us.archive.ubuntu.com/ubuntu/ lucid-updates main restricted universe multiverse 
deb-src http://us.archive.ubuntu.com/ubuntu/ lucid-proposed main restricted universe multiverse 
deb-src http://us.archive.ubuntu.com/ubuntu/ lucid-backports main restricted universe multiverse 

###### Ubuntu Partner Repo
deb http://archive.canonical.com/ubuntu lucid partner
deb-src http://archive.canonical.com/ubuntu lucid partner

##############################################################
##################### UNOFFICIAL  REPOS ######################
##############################################################

###### 3rd Party Binary Repos

#### Webmin - http://www.webmin.com
## Run this command: wget http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
deb http://download.webmin.com/download/repository sarge contrib
##############################################################
<<

# Добавление ключей для репозиториев
wget http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
dig -t A security.ubuntu.com

# Обновление данных о пакетах
apt-get update
apt-get upgrade
apt-get -f install

# Установка зависимостей Webmina
apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl 

# Установка жизненноважных пакетов
apt-get install mc ssh openssh-server openssh-client webmin

# Запасной вариант установки Webmina
cd /tmp
wget http://prdownloads.sourceforge.net/webadmin/webmin_1.520_all.deb
dpkg -i webmin_1.520_all.deb

# Установка основных серверных приложений
apt-get install pptpd ipx ppp pptp-linux squid3 squidclient iptraf htop curl cmake cpp-4.3 bcrelay cpp-4.1 ipx gcc-4.1 g++-4.1 ipgrab iptraf gcc-4.3 gcc htop g++-4.3 gcc-4.3-base g++ gcc-4.1-base iproute ntp ntpdate make mcrypt makedev libpcre3-dev php5-mcrypt traceroute calamaris libgd-graph-perl libnetaddr-ip-perl oops

# Установка ipxripd в ручную
cd /tmp
wget http://ubuntu.secs.oakland.edu/pool/universe/i/ipxripd/ipxripd_0.7-13.1_i386.deb
dpkg -i ipxripd_0.7-13.1_i386.deb

apt-get autoremove