postRegisterCtrl.controller 'PostRegisterModelStep3Ctrl', [
  '$scope'
  '$translate'
  '$state'
  '$stateParams'
  '$controller'
  '$timeout'
  '$localStorage'
  'Auth'
  'PhotoResource'
  'ModelResource'
  'Restangular'
  ($scope, $translate, $state, $stateParams, $controller, $timeout,
   $localStorage, Auth, PhotoResource, ModelResource, Restangular)->

    $controller 'PostRegisterHeaderCtrl', $scope: $scope

    $scope.app.pageTitle = $translate.instant 'url.auth.postregistration.models.step3'
    $scope.currentUserEdit = $stateParams.profile_id
    $scope.modelProfiles = []
    $scope.images = [{}]

    onePhoto = -> Restangular.all('photos')
    allPhotos = -> Restangular.one('photos')

    $scope.scrollToTab()

    $scope.go_back = ->
      $state.go "#{$scope.app.lang}.postregistration.model.step.second.profile", profile_id: 0

    $scope.go_next = ->
      ModelResource.one().get().then (q) ->
        if q.photos_status is "pending"
          if $scope.app.status is "rejected"
            return $state.go "#{$scope.app.lang}.postregistration.model.step.fifth.profile", profile_id: 0
          else
            return $state.go "#{$scope.app.lang}.postregistration.model.step.fourth.profile", profile_id: 0
        else
          $scope.message.error $translate.instant('model.photo_need_more')

    $scope.imageUploaded = (data, image) ->
      error = -> $scope.message.error $translate.instant('server.error')
      success = -> $scope.message.success $translate.instant("model.photo_has_been_saved")
      if image.id?
        onePhoto().withHttpConfig({transformRequest: angular.identity})
          .customPUT(data, image.id, undefined, {'Content-Type': undefined})
          .then success, error
      else
        $scope.images.push onePhoto()
        $scope.$broadcast 'image-uploader-init'
        onePhoto().withHttpConfig({transformRequest: angular.identity})
          .customPOST(data, image.id, undefined, {'Content-Type': undefined})
          .then success, error

    $scope.getImage = (i, image)->
      return $scope.images[i].data if $scope.images[i]? and $scope.images[i].data?
      return $scope.images[i].img if $scope.images[i]? and $scope.images[i].img?
      return "/img/1x1.png"

    allPhotos().getList().then (q) ->
      $scope.images = q
      q.sort (a, b) ->
        if a.id > b.id
          return 1
        if a.id < b.id
          return -1
        return 0
      $scope.images.push onePhoto()
      $scope.$broadcast 'image-uploader-init'
]
