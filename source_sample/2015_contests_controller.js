module.exports = function (app, userdata, models, async) {
  app.get('/contests', function (req, res, next) {
    async.parallel(
			{
				notification: function (callback) { models.contests.find({ 'status': 'notification' }, function (err, contest) { callback(null, contest); }) },
				inprogress: function (callback) { models.contests.find({ 'status': 'inprogress' }, function (err, contest) { callback(null, contest); }) },
				complete: function (callback) { models.contests.find({ 'status': 'complete' }, function (err, contest) { callback(null, contest); }) }
			}, function (err, results) {
				
				var userId = undefined; if (req.user != undefined) { userId = req.user.id; }
				
				userdata(userId, function(err, user, locals){
					res.render('contests.html', {
								locals: locals, 
								user: user, 
								notification: results.notification,
								inprogress: results.inprogress,
								complete: results.complete,
								info: { page: 'contests', title: 'Конкурсы' }
					});
				});
			});
  });

  app.get('/contest/:id', function (req, res, next) {
	  
		var userId = undefined; if (req.user != undefined) { userId = req.user.id; }
		
		models.contests.findById(req.params.id, function (err, contest) {
			userdata(userId, function(err, user, locals){
				res.render('contest.html', { locals: locals, user: user, contests: contest, info: { page: 'contests', title: contest.name } });
			})
		});
  	});
};