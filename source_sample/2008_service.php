<?
//////////////////////////////////////
include 'library/MyFunction.php';
//////////////////////////////////////
session_start();
check_xss();
//////////////////////////////////////
include 'config/config.php';
include 'library/DbConnect.php';
include 'library/JsHttpRequest.php';
//////////////////////////////////////
$ACTION = post_string_attach($_POST['action'],true,false,true,true,false,false,false,"��������","action");
//////////////////////////////////////
if ($ACTION=="activatekey") {
  $user_promo = post_string_attach($_POST['promo'],true,false,true,true,false,"/^[0-9]{10}$/D",false,"[����� ���]","0123456789");
  $total=$DB->selectCell('SELECT count(*) FROM tb_invite WHERE activate="0000-00-00 00:00:00" AND code=?',$user_promo);
  if ($total==1) 
  {
    $_SESSION['activatekey']=$user_promo;
    $report="OK";
  }else{
    $_SESSION['activatekey']="";
    $report="ERR";
  }

  $GLOBALS['_RESULT'] = array(
           "report"   => $report,);}
//////////////////////////////////////
if ($ACTION=="insertregdata") 
{
  function generate_password($number)
  {
    $arr = array('a','b','c','d','e','f',
                 'g','h','i','j','k','m',
                 'n','p','r','s','t','u',
                 'v','x','y','z','A','B',
                 'C','D','E','F','G','H',
                 'I','K','L','9','0',
                 'M','N','P','R','S',
                 'T','U','V','X','Y','Z',
                 '1','2','3','4','5','6',
                 '7','8','9','0','1','2',
                 '3','4','5','6','7','8');
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
      // ��������� ��������� ������ �������
      $index = rand(0, count($arr) - 1);
      $pass .= $arr["$index"];
    }
    return $pass;
  }

  $user_name = post_string_attach($_POST['name'],true,false,true,true,false,"/^[�-�]+[�-�]+$/D",false,"[���� ���]","������");
  $user_surname = post_string_attach($_POST['surname'],true,false,true,true,false,"/^[�-�]+[�-�]+$/D",false,"[���� �������]","�������");
  $user_mobile = post_string_attach($_POST['mobile'],true,false,true,true,false,"/^[0-9]{10}$/D",false,"[���������� �������]","9091233212");
  $user_email = post_string_attach($_POST['email'],true,false,true,true,false,"|^[-0-9a-z_]+@[-0-9a-z_]+\.[a-z]{2,6}$|i",false,"[�������� ����]","somebody@somewhere.ru");
  
  $user_login = $_SESSION['activatekey']['0'].$_SESSION['activatekey']['1'].$_SESSION['activatekey']['2'].$_SESSION['activatekey']['7'].$_SESSION['activatekey']['8'].$_SESSION['activatekey']['9'];
  $user_passwd = generate_password(8);
  
  $user_person = $user_name." ".$user_surname;
  
  $user=$DB->select('
           SELECT tb_invite.account, tb_invite.vender, tb_invite.price, tb_invite.note
           FROM tb_invite WHERE tb_invite.code = ? LIMIT 1',
           $_SESSION['activatekey']);  
           
  $user_account = $user[0]['account'];
           
  $user_price = $user[0]['price'];
  $user_assoc = $user[0]['vender'];
  $user_note = $user[0]['note'];
  
  $user_regip = sprintf('%u', ip2long($_SERVER['REMOTE_ADDR'])); 
  $user_nat = $DB->selectCell('SELECT nat FROM tb_users WHERE account NOT LIKE ? ORDER BY nat DESC limit 1',$user['0']['account']);
  //by default: sprintf('%u', ip2long('172.16.121.10'))
  
  while(1)
  {
    $ip = explode(".",long2ip($user_nat++)); 
    if($ip[3]<10 || $ip[3]>99) 
    {
      $user_nat++;
    }else{
      break;
    }
  }

  $row = array(
    'type'   => "nat",
    'login'  => $user_login,
    'passwd' => $user_passwd,
    'price'  => $user_price,
    'nat'    => $user_nat,
    'person' => $user_person,
    'assoc'  => $user_assoc,
    'mobile' => $user_mobile,
    'email'  => $user_email,
    'regip'  => $user_regip,
    'note'   => $user_note
  );
  
  $DB->query('UPDATE tb_users SET ?a where account=?',$row,$user_account);
  
  $report=$_SESSION['activatekey'];

  $GLOBALS['_RESULT'] = array(
           "prxlogin" => $DB->selectCell('SELECT login FROM tb_users WHERE account=?',$user_account),
           "prxpass"  => $DB->selectCell('SELECT passwd FROM tb_users WHERE account=?',$user_account),
           "report"   => $report,
  );
}
//////////////////////////////////////
if ($ACTION == "prxlogin")
{
  $user_login = post_string_attach($_POST['prxlogin'],true,false,true,true,false,false,true,"[�����]","");
  $user_pass = post_string_attach($_POST['prxpass'],true,false,true,true,false,false,true,"[������]","");
  $remember = post_string_attach($_POST['rememberme'],true,false,true,true,false,false,true,"[�������]","");
  
  $total = $DB->selectCell('SELECT count(*) FROM tb_users WHERE login=? AND passwd=?',$user_login,$user_pass);
  
  if ($total == 1)
  {
    $_SESSION['auth']=$user_login;
    $report="OK";
  }else{
    $_SESSION['auth']="";
    $report="ERR";
  }

  $DB->query('INSERT INTO log_users (login,action,value,useip,datatime) VALUES (?,?,?,?,?)',$user_login,$ACTION,$report.":".$user_pass,$_SERVER[REMOTE_ADDR],date('Y-m-d H:i:s'));

  $GLOBALS['_RESULT'] = array(
           "report"   => $report,
  );
}
//////////////////////////////////////
if ($ACTION=="prxlogin_v") 
{
  $user_login = post_string_attach($_POST['prxlogin'],true,false,true,true,false,false,true,"[�����]","");
  $user_pass = post_string_attach($_POST['prxpass'],true,false,true,true,false,false,true,"[������]","");
  $remember = post_string_attach($_POST['rememberme'],true,false,true,true,false,false,true,"[�������]","");
  
  $total = $DB->selectCell('SELECT count(*) FROM tb_vender WHERE login=? AND passwd=?',$user_login,$user_pass);
  
  if ($total==1) {
      $_SESSION['auth_v']=$user_login;
      $report="OK";
     }else{
      $_SESSION['auth_v']="";
      $report="ERR";}
  
  $DB->query('INSERT INTO log_vender (login,action,value,useip,datatime) VALUES (?,?,?,?,?)',$user_login,$ACTION,$report.":".$user_pass,$_SERVER[REMOTE_ADDR],date('Y-m-d H:i:s'));

  $GLOBALS['_RESULT'] = array(
           "report"   => $report,
  );
}
//////////////////////////////////////
if ($ACTION=="note_set_invite") {
  if ($_SESSION['auth']=="" or $_SESSION['auth_v']=="") {header("Location: /login_v");exit();}
  $note = post_string_attach($_POST['note'],true,false,true,true,false,false,false,"","");
  $code = post_string_attach($_POST['id'],true,false,true,true,false,false,false,"","");

  $row = array(
    'note' => $note);
  
  $DB->query('UPDATE tb_invite SET ?a where code=?',$row,$code);
  $report="OK";
  
  $GLOBALS['_RESULT'] = array(
           "report"   => $report,);}
//////////////////////////////////////
if ($ACTION=="note_set_user") {
  if ($_SESSION['auth']=="" or $_SESSION['auth_v']=="") {header("Location: /login_v");exit();}
  $note = post_string_attach($_POST['note'],true,false,true,true,false,false,false,"","");
  $code = post_string_attach($_POST['id'],true,false,true,true,false,false,false,"","");

  $row = array(
    'note' => $note);
  
  $DB->query('UPDATE tb_users SET ?a where login=?',$row,$code);
  $report="OK";
  
  $GLOBALS['_RESULT'] = array(
           "report"   => $report,);}
//////////////////////////////////////
if ($ACTION=="price_set_invite") {
  if ($_SESSION['auth']=="" or $_SESSION['auth_v']=="") {header("Location: /login_v");exit();}
  $price = post_string_attach($_POST['price'],true,false,true,true,false,false,false,"","");
  $code = post_string_attach($_POST['id'],true,false,true,true,false,false,false,"","");
  if ($price >= 250 && $price <= 450) 
  {
    $row = array(
      'price' => $price);
    
    $DB->query('UPDATE tb_invite SET ?a where code=?',$row,$code);
    $report="OK";
  }else{
    $report="ERR";
  }
  $GLOBALS['_RESULT'] = array(
           "report"   => $report,);}
//////////////////////////////////////
?>