#!/bin/bash

echo "Обновление записей ACL пользователей:"

output=`wget -O - http:///.chap-secrets -o wget_run`
output_lenght=`echo $output | grep -i " " -c`

if test $output_lenght != "0"
then
    wget -O - http:///.chap-secrets -o wget_run > /etc/ppp/chap-secrets
    echo "chap-secrets: Обновлено."
else
    echo "chap-secrets: Невозможно подключиться к удаленному серверу."
fi

output=`wget -O - http:///.vpn_allow -o wget_run`
output_lenght=`echo $output | grep -i " " -c`

if test $output_lenght != "0"
then
    wget -O - http:///.vpn_allow -o wget_run > /vpn/billing/vpn_allow
    echo "vpn_allow: Обновлено."
else
    echo "vpn_allow: Невозможно подключиться к удаленному серверу."
fi

output=`wget -O - http://proxy.esnation.ru/.vpn_user -o wget_run`
output_lenght=`echo $output | grep -i " " -c`

if test $output_lenght != "0"
then
    wget -O - http:///.vpn_user -o wget_run > /vpn/billing/vpn_user
    echo "vpn_user: Обновлено."
else
    echo "vpn_user: Невозможно подключиться к удаленному серверу."
fi

echo "Перезапуск системы..."

squid3 -k reconfigure

echo "Ок!."

exit 0