﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace edolymp_bot
{
    public partial class Form1 : Form
    {
        public MongoDatabase db;
        public MongoServer server;
        public MongoClient client;

        private Question quest;

        string questionHTML, questionTXT, answerTRUE, answerFALSE, currentANSWER, currentHASH, dbANSWER;

        double start;

        MD5 md5Hash = MD5.Create();

        public Form1()
        {
            InitializeComponent();
        }

        public void UpdateQuestion(Question quest)
        {
            try
            {
                var collection = db.GetCollection<Question>("question");

                var query = Query<Question>.EQ(e => e._hash, quest._hash);

                if (collection.FindAs<Question>(query).Count() > 0)
                {
                    var update = Update<Question>.Set(e => e._answer, quest._answer);
                    collection.Update(query, update);
                }
                else
                {
                    collection.Save(quest);
                }
            }
            catch (Exception)
            {


            }
        }

        public string GetAnswer(string hash)
        {
            try
            {
                var collection = db.GetCollection<Question>("question");

                var query = Query<Question>.EQ(e => e._hash, hash);

                if (collection.FindAs<Question>(query).Count() > 0)
                {
                    return collection.FindOne(query)._answer;
                }
                else 
                {
                    return "9999";
                }
            }
            catch (Exception)
            {
                
                
            }
          
            return "9999";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var host = "127.0.0.1";
            var connectionString = "mongodb://" + host + "/edolymp";
            client = new MongoClient(connectionString);
            server = client.GetServer();
            db = server.GetDatabase("edolymp");

            webControl1.Source = new Uri("https://edolymp.ru/");
            listBox2.Items.Insert(0, item: "new uri " + webControl1.Source.ToString());
        }

        private void Awesomium_Windows_Forms_WebControl_LoadingFrameComplete(object sender, Awesomium.Core.FrameEventArgs e)
        {
            listBox2.Items.Insert(0, item: "complete " + webControl1.Source.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                webControl1.ExecuteJavascript("$('#passport_type_login_username').val('')");
                webControl1.ExecuteJavascript("$('#passport_type_login_password').val('')");
                webControl1.ExecuteJavascript("$('#passport_type_login_submit').click()");
            }
            catch (Exception)
            {


            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                webControl1.Source = new Uri(textBox1.Text);
                listBox2.Items.Insert(0, item: "new game");
            }
            catch (Exception)
            {


            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            listBox1.Items.Insert(0, item: "timer is ON");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            listBox1.Items.Insert(0, item: "timer is OFF");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (start < 1)
                { 
                    label1.Text = "0";
                    panel4.Visible = false;
                }
                else
                { 
                    label1.Text = start.ToString();
                    panel4.Visible = true;
                }

                start = start - 500;

            if (webControl1.IsDocumentReady)
            {
                questionHTML = webControl1.ExecuteJavascriptWithResult("$('.question').html()");
                questionTXT = webControl1.ExecuteJavascriptWithResult("$('.question').text()");

                answerTRUE = webControl1.ExecuteJavascriptWithResult("$('.battle__answer_answered_true').text()");
                answerFALSE = webControl1.ExecuteJavascriptWithResult("$('.battle__answer_answered_false').text()");

                if (currentHASH != GetMd5Hash(md5Hash, questionHTML))
                {
                    currentHASH = GetMd5Hash(md5Hash, questionHTML);

                    if (questionTXT != "")
                    {
                        button5.ForeColor = Color.Black;
                        button6.ForeColor = Color.Black;
                        button7.ForeColor = Color.Black;
                        button8.ForeColor = Color.Black;

                        button5.BackColor = SystemColors.Control;
                        button6.BackColor = SystemColors.Control;
                        button7.BackColor = SystemColors.Control;
                        button8.BackColor = SystemColors.Control;


                        dbANSWER = GetAnswer(currentHASH);
                        currentANSWER = GetAnswerNo(dbANSWER);
                        

                        if (dbANSWER.IndexOf("1") >= 0)
                        {
                            listBox1.Items.Insert(0, item: "question: 100%");
                        }
                        else 
                        {
                            listBox1.Items.Insert(0, item: "question: " + dbANSWER);
                        }

                        listBox2.Items.Insert(0, item: currentHASH);

                        char[] aa = GetAnswer(currentHASH).ToCharArray();

                        if (aa[0] == Char.Parse("1")) button5.BackColor = Color.Green;
                        if (aa[1] == Char.Parse("1")) button6.BackColor = Color.Green;
                        if (aa[2] == Char.Parse("1")) button7.BackColor = Color.Green;
                        if (aa[3] == Char.Parse("1")) button8.BackColor = Color.Green;

                        if (aa[0] == Char.Parse("0")) button5.ForeColor = Color.Red;
                        if (aa[1] == Char.Parse("0")) button6.ForeColor = Color.Red;
                        if (aa[2] == Char.Parse("0")) button7.ForeColor = Color.Red;
                        if (aa[3] == Char.Parse("0")) button8.ForeColor = Color.Red;

                        aa = currentANSWER.ToCharArray();

                        if (aa[0] == Char.Parse("8")) button5.ForeColor = Color.Blue;
                        if (aa[1] == Char.Parse("8")) button6.ForeColor = Color.Blue;
                        if (aa[2] == Char.Parse("8")) button7.ForeColor = Color.Blue;
                        if (aa[3] == Char.Parse("8")) button8.ForeColor = Color.Blue;

                        linkLabel1.Text = currentANSWER;

                        timer2.Enabled = true;

                        if (checkBox1.Checked == true)
                        { 
                            timer2.Interval = 30000;
                            start = 30000;
                        }
                        else
                        { 
                            timer2.Interval = 100;
                            start = 0;
                        }
                    }
                }

                if (answerTRUE != "")
                {
                    currentANSWER = linkLabel1.Text;

                    if (listBox1.Items[0].ToString() != "FUCK YEA")
                    { 
                        quest = new Question(questionTXT, questionHTML, currentHASH, BuildAnswer(currentANSWER, true));
                        UpdateQuestion(quest);
                        listBox1.Items.Insert(0, item: "FUCK YEA");
                    }
                }

                if (answerFALSE != "")
                {
                    currentANSWER = linkLabel1.Text;

                    if (listBox1.Items[0].ToString() != "oh NO!")
                    { 
                        quest = new Question(questionTXT, questionHTML, currentHASH, BuildAnswer(currentANSWER, false));
                        UpdateQuestion(quest);
                        listBox1.Items.Insert(0, item: "oh NO!");
                    }
                }

                if (webControl1.ExecuteJavascriptWithResult("$('.battle__replay').text()") == "Играть ещё") 
                {
                    if (listBox1.Items[0].ToString() != "new game")
                    { 
                        listBox1.Items.Insert(0, item: "new game");
                        timer4.Enabled = true;
                    }
                }
            }
                        }
            catch (Exception)
            {


            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            string hashOfInput = GetMd5Hash(md5Hash, input);

            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static string GetAnswerNo(string LastAnswer)
        {
            Random rnd = new Random();
            int pos;
            char[] aa = LastAnswer.ToCharArray();

            if (LastAnswer.IndexOf("1") >= 0)
            {
                return LastAnswer.Replace("1", "8"); ;
            }
            else 
            {
                while (LastAnswer.IndexOf("8") < 0)
                {
                    pos = rnd.Next(4);
                    if (aa[pos] == Char.Parse("9"))
                    {
                        aa[pos] = Char.Parse("8");
                    }

                    LastAnswer = new string(aa);
                }
            }

            return LastAnswer;
        }

        static string BuildAnswer(string LastAnswer, bool answer)
        {
            if (answer)
            {
                return LastAnswer.Replace("8", "1");
            }
            else 
            {
                return LastAnswer.Replace("8", "0");
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                webControl1.ExecuteJavascript("$('.battle__answer[data-id=" + linkLabel1.Text.IndexOf("8") + "]').click()");
                timer2.Enabled = false;
            }
            catch (Exception)
            {
                
                
            }
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            char[] aa = linkLabel1.Text.Replace("8", "9").ToCharArray();
            aa[0] = Char.Parse("8");
            linkLabel1.Text = new string(aa);
            timer2.Interval = 1;
            start = 0;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            char[] aa = linkLabel1.Text.Replace("8", "9").ToCharArray();
            aa[1] = Char.Parse("8");
            linkLabel1.Text = new string(aa);
            timer2.Interval = 1;
            start = 0;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            char[] aa = linkLabel1.Text.Replace("8", "9").ToCharArray();
            aa[2] = Char.Parse("8");
            linkLabel1.Text = new string(aa);
            timer2.Interval = 1;
            start = 0;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            char[] aa = linkLabel1.Text.Replace("8", "9").ToCharArray();
            aa[3] = Char.Parse("8");
            linkLabel1.Text = new string(aa);
            timer2.Interval = 1;
            start = 0;
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (listBox2.Items[0].ToString() == "------")
            {
                webControl1.Source = new Uri(textBox1.Text);
            }

            listBox2.Items.Insert(0, item: "------");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text = "https://edolymp.ru/round1/challenge/fast";
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            webControl1.Source = new Uri(textBox1.Text);
            timer4.Enabled = false;
        }

    }


    public class Question
    {
        public ObjectId Id { get; set; }
        public string _txt { set; get; }
        public string _html { set; get; }
        public string _hash { set; get; }
        public string _answer { set; get; }

        public Question(string txt, string html, string hash, string answer)
        {
            this._txt = txt;
            this._html = html;
            this._hash = hash;
            this._answer = answer;
        }
    }

}
