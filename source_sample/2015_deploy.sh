LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse @{u})
BASE=$(git merge-base @ @{u})

cd /home/v3new/project_name/client

if [ $LOCAL = $BASE ]; then
  git checkout test
  git pull
  export NODE_ENV=production
  npm install
  bower install
  gulp c
fi
