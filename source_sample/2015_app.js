var config = require("nconf");
var express = require('express');

var mongoose = require('mongoose');
var passport = require('passport');
var request = require('request');
var crypto = require("crypto");
var async = require('async');

config.argv().env().file({ file: 'config.json' });

	var app = express();
	var models = {};
	
	require("./boot/mongoose")(config, mongoose, models);
	require("./boot/express")(app, config, passport);
	require("./boot/passport")(app, config, passport, models);
	
	var mailer = require("./boot/nodemailer")(config);
	var userdata = require("./boot/userdata")(app, models);
	
	require('./routes/index')(app, models, userdata, async);
	require('./routes/api/index')(app, config, models, mongoose, async, request, crypto);

module.exports = app;