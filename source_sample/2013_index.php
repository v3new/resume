<?

include('configs/config.php');
include('libs/smarty.php');
include('libs/dbsimple.php');
include('libs/function.php');
include('libs/htmldom.php');

session_start();

if (!empty($_SESSION['login'])) $smarty->assign('is_admin',"true"); 

//# menu
//$smarty->assign('current_page',$_SERVER['REQUEST_URI']);                       
//$smarty->assign('menu_items',array(0 => array('ref' => 'rs', 'href' => '/page/website/', 'label' => 'Разработка сайтов'),
//                                   1 => array('ref' => 'it', 'href' => '/page/it/', 'label' => 'Информационные технологии'),
//                                   2 => array('ref' => 'dp', 'href' => '/page/print/', 'label' => 'Дизайн полиграфии'),
//                                   3 => array('ref' => 'rr', 'href' => '/page/promotion/', 'label' => 'Рекламные решения')
//                                  )
//               );

# content  
include('modules/module.php');
include('static/signature.php');

$contents = new content;
 
if (isset($_GET['action']) and $_GET['action']=='admin')
{
  include('modules/admin.php');
  exit($contents->built());  
}
 
if ($_SERVER['REQUEST_URI']=='/')
{
  include('modules/index.php');
  $smarty->assign('page','index');  
  $smarty->assign('content',$contents->built());     
}

if ($_GET['page']=='manufacture')
{
  include('modules/manufacture.php');
  $smarty->assign('page','manufacture'); 
  $smarty->assign('content',$contents->built());     
}

if ($_GET['page']=='about')
{
  include('modules/about.php');
  $smarty->assign('page','about'); 
  $smarty->assign('content',$contents->built());     
}

if ($_GET['page']=='contact')
{
  include('modules/contact.php');
  $smarty->assign('page','contact'); 
  $smarty->assign('content',$contents->built());     
}

if (isset($_GET['action']) and $_GET['action']=='dynamic')
{
  include('static/pages.php');
  $smarty->assign('content',$contents->built());  
}

include('modules/layout.php');
$smarty->display('layout.tpl');

?>